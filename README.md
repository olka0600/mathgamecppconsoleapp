# Math Game
This is a simple Math Game built in C++. The purpose of the game is to practice mental arithmetic. 
The game is a console application ... for now.

## How to use it
Clone the repository and build it using CMake.

### Contribution guidelines ###
If anyone feel like contributing feel free to contact me and I add you to the access group.

### Who do I talk to? ###
olka0600@student.miun.se