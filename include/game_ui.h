#pragma once
#include "game_logic.h"

class GameUI {
 public:
  void Run();
 private:
  GameLogic gameLogic;
  void printMenuOptions();
  bool menuRuns();
  int getChoice(const std::string &prompt, int lower, int upper);
  void displayAdditon();
  void displaySubtraction();
};