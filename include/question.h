#pragma once
#include <string>

class Question {
public:
    Question() = default;
    virtual std::string GetQuestion() const = 0;
    virtual std::string ToString() const = 0;
};
