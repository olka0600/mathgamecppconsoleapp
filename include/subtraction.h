#pragma once

#include <string>
#include "question.h"
#include <random>
#include <chrono>

class Subtraction : Question {
 public:
  Subtraction() = default;
  Subtraction(const Subtraction &aq);

  Subtraction(int _number1, int _number2);

  std::string GetQuestion() const;

  int GetCorrectAnswer() const { return correctAnswer; }

  int GetHighestNumber() const;

  int GetSmallestNumber() const;

  bool IsCorrect(int _userAnswer);

  std::string ToString() const;

 private:
  int correctAnswer;
  int number1;
  int number2;
};

// Functor
class GenerateSubtractionQuestion {
 public:
  GenerateSubtractionQuestion(int _level);
  //  Addition operator()();
  Subtraction GetQuestion();
 private:
  int getRandom();
  void calculateBounds();
  int level;
  int lower;
  int upper;
  int total;
  std::default_random_engine engine;
};