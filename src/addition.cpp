#include "addition.h"

#include <string>
#include <sstream>

Addition::Addition(const Addition &aq) : number1(
    aq.number1), number2(aq.number2) {
  correctAnswer = number1 + number2;
}

Addition::Addition(int _number1, int _number2) : number1(
    _number1), number2(_number2) {
  correctAnswer = number1 + number2;
}

std::string Addition::GetQuestion() const {
  std::ostringstream oss;
  oss << number1 << " + " << number2 << " = ";
  return oss.str();
}

int Addition::GetHighestNumber() const {
  return (number1 > number2) ? number1 : number2;
}

int Addition::GetSmallestNumber() const {
  return (number1 < number2) ? number1 : number2;
}

bool Addition::IsCorrect(int _userAnswer) {
  return correctAnswer == _userAnswer;
}

std::string Addition::ToString() const {
  std::ostringstream oss;
  oss << number1 << ";+;" << number2
      << ";" << correctAnswer;
  return oss.str();
}

GenerateAdditionQuestion::GenerateAdditionQuestion(int _level)
    : level(_level), lower(0), upper(0), total(0) {
  calculateBounds();
  engine.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());

}

Addition GenerateAdditionQuestion::GetQuestion() {
  int number1 = getRandom();
  int number2 = getRandom();
  Addition add(number1, number2);
  return add;
}

int GenerateAdditionQuestion::getRandom() {
  std::uniform_int_distribution<int> distribution(lower, upper);
  return distribution(engine);
}
void GenerateAdditionQuestion::calculateBounds() {
  if (level < 11) {
    lower = 0;
    upper = level;
  } else {
    lower = level - level / 2;
    upper = level;
  }

}
