#include "subtraction.h"

#include <string>
#include <sstream>

Subtraction::Subtraction(const Subtraction &aq)
    : number1(
    aq.number1), number2(aq.number2) {
  correctAnswer = number1 - number2;
}

Subtraction::Subtraction(int _number1, int _number2)
    : number1(
    _number1), number2(_number2) {
  correctAnswer = number1 - number2;
}

std::string Subtraction::GetQuestion() const {
  std::ostringstream oss;
  oss << number1 << " - " << number2 << " = ";
  return oss.str();
}

int Subtraction::GetHighestNumber() const {
  return (number1 > number2) ? number1 : number2;
}

int Subtraction::GetSmallestNumber() const {
  return (number1 < number2) ? number1 : number2;
}

bool Subtraction::IsCorrect(int _userAnswer) {
  return correctAnswer == _userAnswer;
}

std::string Subtraction::ToString() const {
  std::ostringstream oss;
  oss << number1 << ";-;" << number2
      << ";" << correctAnswer;
  return oss.str();
}

GenerateSubtractionQuestion::GenerateSubtractionQuestion(int _level)
    : level(_level), lower(0), upper(0), total(0) {
  calculateBounds();
  engine.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());

}

Subtraction GenerateSubtractionQuestion::GetQuestion() {
  int number1 = getRandom();
  int number2 = getRandom();
  Subtraction sub(number1 + number2, number2);
  return sub;
}


int GenerateSubtractionQuestion::getRandom() {
  std::uniform_int_distribution<int> distribution(lower, upper);
  return distribution(engine);
}
void GenerateSubtractionQuestion::calculateBounds() {
  if (level < 11) {
    lower = 0;
    upper = level;
  } else {
    lower = level - level / 2;
    upper = level;
  }

}
