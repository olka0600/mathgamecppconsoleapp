#include "game_ui.h"

#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

#include "addition.h"
#include "subtraction.h"
#include "file_handler.h"
#include "log.h"

void GameUI::Run() {
  do {
    printMenuOptions();
  } while (menuRuns());
}

void GameUI::printMenuOptions() {
  std::cout << "--- MATH GAME ---\n\n[1] Addition\n[2] Subtraction\n"
               "[3] Multiplication\n[4] Division\n[5] Mix Math\n[6] Quit\n";
}

bool GameUI::menuRuns() {
  int choice = getChoice(":", 1, 6);

  switch (choice) {
    case 1:
      displayAdditon();
      break;
    case 2:
      displaySubtraction();
      break;
    case 3:
      gameLogic.Multiplication();
      break;
    case 4:
      gameLogic.Division();
      break;
    case 5:
      gameLogic.Mix();
      break;
    case 6:
      std::cout << "Bye\n";
      return false;
  }
  return true;
}

int GameUI::getChoice(const std::string &prompt, int lower, int upper) {
  auto clear = []() {
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
  };
  do {
    std::cout << prompt;
    int choice = 0;
    std::cin >> choice;
    if (!std::cin.fail() && choice >= lower && choice <= upper) {
      clear();
      return choice;
    } else {
      clear();
    }
  } while (true);
}
void GameUI::displayAdditon() {
  int level = 1;
  std::vector<int> durations;
  int speedCapacity;
  for (int i = 0; i < 50; ++i) {
    std::cout << level << "\n";
    if (durations.size() > 3) {
      speedCapacity =
          (durations[1] + durations[2] + durations[3]) / 3;
      int momentum =
          (durations[durations.size() - 1] + durations[durations.size() - 2]) /
          2;
      if (momentum < speedCapacity + 2000) level++;
      if (momentum > speedCapacity + 5000) level--;
    }
    GenerateAdditionQuestion generateAdditionQuestion(level);
    Addition addition(generateAdditionQuestion.GetQuestion());
    auto start = std::chrono::high_resolution_clock::now();
    std::cout << addition.GetQuestion();
    int answer = getChoice("", -100000, 100000);
    auto stop = std::chrono::high_resolution_clock::now();
    int duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(stop - start)
            .count();
    if (addition.IsCorrect(answer)) {
      // std::cout << "Bra jobbat!\n";
    } else {
      // std::cout << "Ingen fara, jobba vidare :)!\n";
      duration = speedCapacity + 7000;
    }
    durations.push_back(duration);
  }
}

void GameUI::displaySubtraction() {
  int level = 1;
  std::vector<int> durations;
  int speedCapacity;
  for (int i = 0; i < 50; ++i) {
    std::cout << level << "\n";
    if (durations.size() > 3) {
      speedCapacity =
          (durations[1] + durations[2] + durations[3]) / 3;
      int momentum =
          (durations[durations.size() - 1] + durations[durations.size() - 2]) /
          2;
      if (momentum < speedCapacity + 2000) level++;
      if (momentum > speedCapacity + 5000) level--;
    }
    GenerateSubtractionQuestion generateSubtractionQuestion(level);
    Subtraction subtraction(generateSubtractionQuestion.GetQuestion());
    auto start = std::chrono::high_resolution_clock::now();
    std::cout << subtraction.GetQuestion();
    int answer = getChoice("", -100000, 100000);
    auto stop = std::chrono::high_resolution_clock::now();
    int duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(stop - start)
            .count();
    if (subtraction.IsCorrect(answer)) {
      // std::cout << "Bra jobbat!\n";
    } else {
      // std::cout << "Ingen fara, jobba vidare :)!\n";
      duration = speedCapacity + 7000;
    }
    durations.push_back(duration);
  }
}
