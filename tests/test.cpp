#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "addition.h"

TEST_CASE("Addition", "Test1") {
  Addition aq(1, 2);
  REQUIRE(aq.GetCorrectAnswer() == 3);
  REQUIRE(aq.GetQuestion() == "1 + 2 = ");
  REQUIRE(aq.ToString() == "1;+;2;3");
}
